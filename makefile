CC=gcc
CFLAGS=-std=c11
all: avs_lab_3

avs_lab_3: avs_lab_3.c
	$(CC) $(CFLAGS) avs_lab_3.c -o avs_lab_3

clean:
	rm avs_lab_3
