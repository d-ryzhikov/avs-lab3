#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

extern int errno;

int32_t *calculate_f(int8_t *a, int8_t *b, int8_t *c, int16_t *d);

bool read_from_file(FILE *input, int8_t *a, int8_t *b, int8_t *c, int16_t *d);

bool parse_line_8bit(char *line, int8_t *x);

bool parse_line_16bit(char *line, int16_t *x);

void print_results(int8_t *a, int8_t *b, int8_t *c, int16_t *d, int32_t *f);

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage:\n\t%s FILENAME\n", argv[0]);
        exit(EXIT_SUCCESS);
    }

    FILE *input = fopen(argv[1], "r");

    if (input == NULL) {
        fprintf(stderr, "Error opening file: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    int8_t a[8];
    int8_t b[8];
    int8_t c[8];
    int16_t d[8];

    if (!read_from_file(input, a, b, c, d)) {
        printf("Error reading from file: file contents are not valid.\n");
        exit(EXIT_FAILURE);
    }

    int32_t *f = calculate_f(a, b, c, d);

    print_results(a, b, c, d, f);

    return 0;
}

int32_t *calculate_f(int8_t *a, int8_t *b, int8_t *c, int16_t *d)
{
    static int32_t f[8];

    __asm__ volatile (
        "mov $-1, %%eax\n\t"
        "movd %%eax, %%mm5\n\t"
        "punpckldq %%mm5, %%mm5\n\t"
        "packuswb %%mm5, %%mm5\n\t"         //mm5=8*(-1)
        "movq %%mm5, %%mm2\n\t"
        "movq (%[c]), %%mm0\n\t"            //mm0=c
        "movq %%mm0, %%mm1\n\t"             //mm0=mm1=c
        "pcmpgtb %%mm0, %%mm2\n\t"          //save signs of c elements
        "pxor %%mm5, %%mm2\n\t"
        "punpcklbw %%mm2, %%mm0\n\t"        //mm0=c[1..4]
        "punpckhbw %%mm2, %%mm1\n\t"        //mm1=c[5..8]
        "movq (%[b]), %%mm2\n\t"            //mm2=b
        "movq %%mm2, %%mm3\n\t"             //mm3=mm2=b
        "movq %%mm5, %%mm4\n\t"
        "pcmpgtb %%mm2, %%mm4\n\t"          //save signs of b elements
        "pxor %%mm5, %%mm4\n\t"
        "punpcklbw %%mm4, %%mm2\n\t"        //mm2=b[1..4]
        "punpckhbw %%mm4, %%mm3\n\t"        //mm3=b[5..8]
        "paddsw %%mm2, %%mm0\n\t"           //mm0=c+b[1..4]
        "paddsw %%mm3, %%mm1\n\t"           //mm1=c+b[5..8]
        "movq (%[a]), %%mm2\n\t"            //mm2=a
        "movq %%mm2, %%mm3\n\t"             //mm3=mm2=a
        "movq %%mm5, %%mm4\n\t"
        "pcmpgtb %%mm2, %%mm4\n\t"          //save signs of a elements
        "pxor %%mm5, %%mm4\n\t"
        "punpcklbw %%mm4, %%mm2\n\t"        //mm2=a[1..4]
        "punpckhbw %%mm4, %%mm3\n\t"        //mm3=a[5..8]
        "pmullw %%mm2, %%mm0\n\t"           //mm0=a*(c+b)[1..4]
        "pmullw %%mm3, %%mm1\n\t"           //mm1=a*(c+b)[5..8]
        "movq %%mm0, %%mm2\n\t"             //mm2=mm0=a*(c+b)[1..4]
        "movq %%mm1, %%mm3\n\t"             //mm3=mm1=a*(c+b)[5..8]
        "pmullw (%[d]), %%mm0\n\t"          //mm0=a*(c+b)*d[1..4](l.o.)
        "pmulhw (%[d]), %%mm2\n\t"          //mm2=a*(c+b)*d[1..4](h.o.)
        "pmullw 8(%[d]), %%mm1\n\t"         //mm1=a*(c+b)*d[5..8](l.o.)
        "pmulhw 8(%[d]), %%mm3\n\t"         //mm3=a*(c+b)*d[5..8](h.o.)
        "movq %%mm0, %%mm4\n\t"             //mm4=mm0
        "punpcklwd %%mm2, %%mm0\n\t"        //mm0=a*(c+b)*d[1,2]
        "punpckhwd %%mm2, %%mm4\n\t"        //mm4=a*(c+b)*d[3,4]
        "movq %%mm1, %%mm2\n\t"             //mm2=mm1
        "punpcklwd %%mm3, %%mm1\n\t"        //mm1=a*(c+b)*d[5,6]
        "punpckhwd %%mm3, %%mm2\n\t"        //mm2=a*(c+b)*d[7,8]
        "movq %%mm0, (%[f])\n\t"            //load f[1,2]
        "movq %%mm4, 8(%[f])\n\t"           //load f[3,4]
        "movq %%mm1, 16(%[f])\n\t"          //load f[5,6]
        "movq %%mm2, 24(%[f])\n\t"          //load f[7,8] 
        "emms\n\t"
        : : [a] "r" (a), [b] "r" (b), [c] "r" (c), [d] "r" (d), [f] "r" (f)
        : "eax", "cx", "memory"
    );

    return f;
}

bool read_from_file(FILE *input, int8_t *a, int8_t *b, int8_t *c, int16_t *d)
{
    char line[256];

    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_8bit(line, a)) {
        return false;
    }
    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_8bit(line, b)) {
        return false;
    }
    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_8bit(line, c)) {
        return false;
    }
    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_16bit(line, d)) {
        return false;
    }
    return true;
}

bool parse_line_8bit(char *line, int8_t *x)
{
    uint8_t c = 0;
    int8_t i = 0;
    int8_t sign;
    uint8_t result; 

    while (((line[c] != '\n') || (line[c] != EOF)) && (i < 8)) {
        result = 0;
        sign = 1;
        while ((line[c] == ' ') || (line[c] == '\t')) {
            ++c;
        }
        if (line[c] == '-') {
            sign = -1;
            ++c;
        }
        if ((line[c] < '0') || (line[c] > '9')) {
            return false;
        }
        while ((line[c] >= '0') && (line[c] <= '9')) {
            result = result * 10 + (line[c] - '0');
            if (result > 128) {
                return false;
            }
            ++c;
        }
        if ((sign > 0) && (result > 127)) {
            return false;
        }
        x[i] = sign * ((int8_t) result);
        ++i;
    }
    if (i < 7) {
        return false;
    }

    return true;
}

bool parse_line_16bit(char *line, int16_t *x)
{
    uint8_t c = 0;
    int8_t i = 0;
    uint16_t result;
    int8_t sign;

    while (((line[c] != '\n') || (line[c] != EOF)) && (i < 8)) {
        result = 0;
        sign = 1;
        while ((line[c] == ' ') || (line[c] == '\t')) {
            ++c;
        }
        if (line[c] == '-') {
            sign = -1;
            ++c;
        }
        if ((line[c] < '0') || (line[c] > '9')) {
            return false;
        }
        while ((line[c] >= '0') && (line[c] <= '9')) {
            result = result * 10 + (line[c] - '0');
            if (result > 32768) {
                return false;
            }
            ++c;
        }
        if ((sign > 0) && (result > 32767)) {
            return false;
        }
        x[i] = sign * ((int16_t) result);
        ++i;
    }
    if (i < 7) {
        return false;
    }

    return true;
}

void print_results(int8_t *a, int8_t *b, int8_t *c, int16_t *d, int32_t *f)
{
    printf("%-2s|%-6s|%-6s|%-6s|%-6s|%-12s\n", "i", "A[i]", "B[i]", "C[i]",
           "D[i]", "F[i]");

    int8_t i = 0; 

    for (; i < 43; i++) {
        printf("=");
    }
    printf("\n");
    for (i = 0; i < 8; i++) {
        printf("%-2d|%-6d|%-6d|%-6d|%-6d|%-12d\n", i, a[i], b[i], c[i], d[i],
               f[i]);
    }
}
